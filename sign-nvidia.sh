#!/bin/bash

# Sign nvidia kernel modules
# suiseiseki 2022 
# "...Nvidia... fuck you!" -Linus Torvalds, 2012

if [ "$EUID" -ne 0 ]
  then echo "Script must be run as root, desu~"
  exit
fi

/usr/src/linux/scripts/sign-file sha512 /usr/src/linux/certs/signing_key.pem /usr/src/linux/certs/signing_key.x509 /lib/modules/$(uname -r)/video/nvidia.ko

/usr/src/linux/scripts/sign-file sha512 /usr/src/linux/certs/signing_key.pem /usr/src/linux/certs/signing_key.x509 /lib/modules/$(uname -r)/video/nvidia-uvm.ko

/usr/src/linux/scripts/sign-file sha512 /usr/src/linux/certs/signing_key.pem /usr/src/linux/certs/signing_key.x509 /lib/modules/$(uname -r)/video/nvidia-modeset.ko

/usr/src/linux/scripts/sign-file sha512 /usr/src/linux/certs/signing_key.pem /usr/src/linux/certs/signing_key.x509 /lib/modules/$(uname -r)/video/nvidia-drm.ko

/usr/src/linux/scripts/sign-file sha512 /usr/src/linux/certs/signing_key.pem /usr/src/linux/certs/signing_key.x509 /lib/modules/$(uname -r)/video/nvidia-peermem.ko

