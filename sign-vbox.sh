#!/bin/bash

# Sign virtualbox kernel modules
# suiseiseki 2022 


if [ "$EUID" -ne 0 ]
  then echo "Script must be run as root, desu~"
  exit
fi

/usr/src/linux/scripts/sign-file sha512 /usr/src/linux/certs/signing_key.pem /usr/src/linux/certs/signing_key.x509 /lib/modules/$(uname -r)/misc/vboxdrv.ko

/usr/src/linux/scripts/sign-file sha512 /usr/src/linux/certs/signing_key.pem /usr/src/linux/certs/signing_key.x509 /lib/modules/$(uname -r)/misc/vboxnetflt.ko

/usr/src/linux/scripts/sign-file sha512 /usr/src/linux/certs/signing_key.pem /usr/src/linux/certs/signing_key.x509 /lib/modules/$(uname -r)/misc/vboxnetadp.ko

